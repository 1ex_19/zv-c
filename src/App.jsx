import { Ani, Frame } from "./c";
import DevWrap from "./c/DevWrap";
// import Frame from "./c/Frame";
import OpenBlock from "./c/OpenBlock";
import ZvReady from "./c/ZvReady";
import Content from "./content/Content";

export default function App(props) {
  return (
    <div className="main-app">
      <ZvReady>
        <DevWrap>
          <Ani distance={"-20px"}>
            <div className="m-2">
              <Frame>
                <Content />
              </Frame>
            </div>
          </Ani>
        </DevWrap>
      </ZvReady>
    </div>
  );
}
