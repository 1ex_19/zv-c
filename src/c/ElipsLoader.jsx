export default function ElipsLoader(props) {
  let style = props.style ?? {};
  let bg = props.bg ?? "#000";

  return (
    <div className="lds-ellipsis">
      <div
        style={{
          background: bg,
          ...style,
        }}
      ></div>
      <div
        style={{
          background: bg,
          ...style,
        }}
      ></div>
      <div
        style={{
          background: bg,
          ...style,
        }}
      ></div>
      <div
        style={{
          background: bg,
          ...style,
        }}
      ></div>
    </div>
  );
}
