import React, { Component } from "react";
import XLSX from "xlsx";

class SaveTable extends Component {
  constructor(props) {
    super(props);
    this.save = this.save.bind(this);
  }

  save(e) {
    e.stopPropagation();
    e.cancelBubble = true;

    var type = "xlsx";
    let table_node = document.getElementById(this.props.t_id);
    var workbook = XLSX.utils.table_to_book(table_node);
    XLSX.writeFile(workbook, this.props.name + ".xlsx");
  }

  render() {
    return (
      <div className="save-table-block" onClick={this.save}>
        {this.props.children}
      </div>
    );
  }
}
export default SaveTable;
