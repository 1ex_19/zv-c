import React, { Component } from "react";

class Ul extends Component {
  constructor(props) {
    super(props);

    this.styles = {
      openStyle: {
        opacity: "1",
        transform: "translateY(0)",
        zIndex: "10",
      },
      closedStyle: {
        opacity: "0",
        transform: "translateY(-50%)",
        zIndex: "-1",
      },
    };

    this.state = {
      // options: options,
      ulStyle: this.styles.closedStyle,
    };

    this.show = this.show.bind(this);
  }

  show() {
    setTimeout(() => {
      this.setState({
        ulStyle: this.styles.openStyle,
      });
    }, 100);
  }

  // hide() {
  //   setTimeout(() => {
  //     this.setState({
  //       ulStyle: this.styles.closedStyle,
  //     })
  //   }, 200);
  // }

  componentDidMount() {
    this.show();
  }

  render() {
    return (
      <ul className="shadow" style={this.state.ulStyle}>
        {this.props.children}
      </ul>
    );
  }
}

class Select extends Component {
  constructor(props) {
    super(props);
    this._input = React.createRef();
    this.styles = {
      openStyle: {
        opacity: "1",
        transform: "translateY(0)",
        zIndex: "10",
      },
      closedStyle: {
        opacity: "0",
        transform: "translateY(-50%)",
        zIndex: "-1",
      },
    };

    // let options = this.props.options && this.props.options.length > 0 && Array.isArray(this.props.options) ? this.props.options : []

    this.state = {
      // options: options,
      ulStyle: this.styles.closedStyle,
      value: this.props.placeholder,
      label: <div className="text-secondary">{this.props.placeholder}</div>,
      name: this.props.name,
      results: [],
    };

    // this.show = this.show.bind(this);
    // this.hide = this.hide.bind(this);
    this.createOption = this.createOption.bind(this);
    this.inputHandle = this.inputHandle.bind(this);
    this.getResults = this.getResults.bind(this);
  }

  async getResults(inputData) {
    this.setState({ loading: true });
    let url = this.props.url;
    let data = this.props.data
      ? { ...this.props.data, ...inputData }
      : inputData;
    // console.log(data);
    // console.log(url);
    let req = await fetch(url, {
      method: "POST",
      body: JSON.stringify(data),
    });
    let res = await req.json();
    if (res.ok) {
      this.setState({
        results: res.data,
        // ulStyle: this.styles.openStyle
      });
    } else {
      this.setState({
        results: [{ label: "no results", value: "no results" }],
      });
    }

    this.setState({ loading: false });
  }

  createOption(item, index) {
    return (
      <li
        key={index}
        onClick={(click) => {
          // console.log(click);
          this._input.current.value = item.value;
          this.props.extraHandle && this.props.extraHandle(item.value);
          this.setState({
            label: item.label,
            value: item.value,
            results: [],
          });
        }}
      >
        {item.label}
      </li>
    );
  }

  blur(i) {
    // this.hide();
  }

  inputHandle(i) {
    let value = i.currentTarget.value;
    if (value.length < 2) {
      this.setState({ results: [] });
      this.props.extraHandle && this.props.extraHandle("");
      return false;
    }

    this.getResults({ input: value });
  }

  focus(i) {
    // this.show();
  }

  render() {
    let options = this.state.results.map(this.createOption);
    let _label = this.state.label;

    let pa = this.props.pa ?? window.zv.s.pa;
    let style = this.props.style ?? {};

    return (
      <div
        className="zv-input"
        style={{
          padding: pa,
          ...style,
        }}
      >
        <div className="select-input-wrap position-relative">
          {this.props.label && (
            <label
              htmlFor={"_select_" + this.props.id}
              style={{
                color: window.zv.c.defLabel,
                marginLeft: ".25rem",
              }}
              className="mb-1 input-label"
            >
              {this.props.ico && (
                <span
                  style={{
                    display: "inline-block",
                    width: "1.1em",
                    height: "100%",
                    marginRight: ".5rem",
                  }}
                >
                  {this.props.ico}
                </span>
              )}
              {this.props.label}
            </label>
          )}
          <input
            autoComplete="off"
            onInput={this.inputHandle}
            ref={this._input}
            type="text"
            name={this.state.name}
            id={"_select_" + this.props.id}
            placeholder={
              this.props.placeholder ? this.props.placeholder : this.props.label
            }
            className="form-control"
            onBlur={this.blur.bind(this)}
            onFocus={this.focus.bind(this)}
          />

          {options.length > 0 && <Ul>{options}</Ul>}
        </div>
      </div>
    );
  }
}

export default Select;
