// import { read } from "fs/promises";
import { useEffect, useState } from "react";

export default function ZvReady(props) {
  const [ready, setReady] = useState(false);

  const zvDefault = {
    s: {
      br: "8px",
      pa: ".75rem",
      bw: "2px",
    },
    c: {
      defFrame: "#f2eeff",
      defLight: "#fff",
      defLabel: "#555",
    },
    api: "https://zv-life.zolotoyvek.ua/mapi/v3/index.html?p=",
  };

  useEffect(() => {
    fetch(process.env.PUBLIC_URL + "/zv.config.json").then((res) => {
      res
        .json()
        .then((json) => {
          window.zv = { ...window.zv, ...json };
          setReady(true);
        })
        .catch(() => {
          console.info("no zv.config loaded");
          console.info("creating default!");
          window.zv = { ...window.zv, ...zvDefault };
          setReady(true);
        });
    });
  }, []);

  return ready && props.children;
}
