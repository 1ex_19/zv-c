export default function Btn(props) {
  let locked = props.locked ?? {};

  let bootType = props.locked.state
    ? props.locked.bootType ?? props.bootType ?? "secondary"
    : props.bootType ?? "secondary";
  let defClass = props.click ? "btn touch-clicked" : "btn";
  let type = props.type ?? "submit";
  let style = props.style ?? {};
  let clickStyle = props.clickStyle ?? {};
  let s = props.click ? clickStyle : style;

  return (
    <button
      style={s}
      type={type}
      disabled={props.locked.state}
      onClick={(e) => props.onClick && props.onClick(e)}
      className={defClass + " btn-" + bootType}
    >
      {props.locked.state
        ? props.locked.text ?? props.children
        : props.children}
    </button>
  );
}
