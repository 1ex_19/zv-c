import React, { Component } from "react";

class Touch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      click: false,
    };

    this.start = this.start.bind(this);
    this.end = this.end.bind(this);
    this.createChilds = this.createChilds.bind(this);
  }

  start() {
    this.setState({
      click: true,
    });
  }

  end() {
    this.setState({
      click: false,
    });
  }

  createChilds(item, index) {
    if (React.isValidElement(item)) {
      return (
        <span
          onMouseUp={this.end}
          onMouseDown={this.start}
          onTouchStart={this.start}
          onTouchEnd={this.end}
          className="touch-wrap"
        >
          {React.cloneElement(item, { click: this.state.click, index: index })}
        </span>
      );
    }
  }

  render() {
    let childs = React.Children.map(this.props.children, this.createChilds);
    return childs;
  }
}

export default Touch;
