export default function TitledParagraf(props) {
  return (
    <div className="col-12">
      <div className="inner-col p-2">
        <div className="one-record">
          {props.title && (
            <div className="title">
              <h5 className="text-secondary">
                {props.title}
                {props.dotted && ":"}
              </h5>
            </div>
          )}
          <div className="inner main-blue-bg p-2">{props.children}</div>
        </div>
      </div>
    </div>
  );
}
