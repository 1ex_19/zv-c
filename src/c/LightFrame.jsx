export default function LightFrame(props) {
  let bg = props.bg ?? window.zv.c.defLight;
  let br = props.br ?? window.zv.s.br;
  let pa = props.pa ?? window.zv.s.pa;
  let style = props.style ?? {};
  let c = props.hover ? "zv-light-frame hover-handler" : "zv-light-frame";

  return (
    <div
      style={{
        background: bg,
        borderRadius: br,
        padding: pa,
        ...style,
      }}
      onClick={(e) => props.onClick && props.onClick(e)}
      className={c}
    >
      {props.children}
    </div>
  );
}
