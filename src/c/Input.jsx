import React, { useState } from "react";

export default function Input(props) {
  const [val, setVal] = useState(props.value ?? "");
  const _input = React.createRef();

  const change = (e) => {
    setVal(e.target.value);

    return props.onChange && props.onChange(e);
  };

  let pa = props.pa ?? window.zv.s.pa;
  let style = props.style ?? {};
  let defClass = " form-control ";
  let c = props.hover ? defClass + " hover-handler " : defClass;
  return (
    <div
      className="zv-input"
      style={{
        padding: pa,
        ...style,
      }}
    >
      {props.label && (
        <label
          style={{
            color: window.zv.c.defLabel,
            marginLeft: ".25rem",
          }}
          htmlFor={props.id}
        >
          {props.label}
        </label>
      )}
      {props.value ? (
        <input
          name={props.name ?? props.id}
          ref={_input}
          type={props.type}
          value={val}
          onChange={change}
          className={c}
        />
      ) : (
        <input
          ref={_input}
          name={props.name ?? props.id}
          type={props.type}
          onChange={(e) => props.onChange && props.onChange(e)}
          className={c}
        />
      )}
    </div>
  );
}
