import React, { useEffect, useState } from "react";

export default function OpenBlock(props) {
  let block = React.createRef();
  const [open, setOpen] = useState(true);
  const [height, setH] = useState(0);
  const [blockStyle, setBS] = useState({
    transition: ".3s",
    overflow: "hidden",
    marginTop: "10px",
    opacity: 1,
  });
  const [icoStyle, setIC] = useState({
    transition: ".3s",
  });

  let icon = (
    <svg
      style={icoStyle}
      xmlns="http://www.w3.org/2000/svg"
      height="1.5em"
      width="1.5em"
      fill="currentColor"
      className="bi bi-caret-down-fill"
      viewBox="0 0 16 16"
    >
      <path d="M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z" />
    </svg>
  );

  // useEffect(()=> {
  //   if (block.current.clientHeight != 0) {
  //     setH(block.current.clientHeight);

  //   }
  //   // setBS({
  //   //   transition: ".3s",
  //   //   overflow: "hidden",
  //   //   height: "0px"
  //   // });
  //   // setOpen(false);
  // });

  const toggle = () => {
    if (open) {
      setBS({
        transition: ".3s",
        overflow: "hidden",
        height: "0px",
        marginTop: "0px",
      });
      setOpen(false);
      setIC({
        transition: ".3s",
      });
    } else {
      setBS({
        transition: ".3s",
        overflow: "hidden",
        marginTop: "10px",
        // height: height + "px"
      });

      setOpen(true);
      setIC({
        transform: "rotateZ(180deg)",
        transition: ".3s",
      });
    }
  };

  return (
    <div className="open-block">
      <div onClick={toggle} className="open-block-title">
        <div className="d-flex justify-content-between align-items-center">
          <span>{props.title}</span>
          <span>{icon}</span>
        </div>
      </div>
      <div style={blockStyle} ref={block} className="data">
        {props.children}
      </div>
    </div>
  );
}
