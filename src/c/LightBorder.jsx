export default function LightBorder(props) {
  let bc = props.bc ?? window.zv.c.defLight;
  let bw = props.bw ?? window.zv.s.bw;
  let br = props.br ?? window.zv.s.br;
  let pa = props.pa ?? window.zv.s.pa;
  let style = props.style ?? {};
  let c = props.hover ? "zv-light-border hover-handler" : "zv-light-border";

  return (
    <div
      style={{
        border: bw + " solid " + bc,
        borderRadius: br,
        padding: pa,
        ...style,
      }}
      className={c}
    >
      {props.children}
    </div>
  );
}
