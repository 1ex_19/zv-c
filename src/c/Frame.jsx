export default function Frame(props) {
  let bg = props.bg ?? window.zv.c.defFrame;
  let br = props.br ?? window.zv.s.br;
  let pa = props.pa ?? window.zv.s.pa;
  let style = props.style ?? {};

  return (
    <div
      style={{
        background: bg,
        borderRadius: br,
        padding: pa,
        ...style,
      }}
      className="zv-frame"
    >
      {props.children}
    </div>
  );
}
