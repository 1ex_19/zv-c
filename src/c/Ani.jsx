import { useEffect, useState } from "react";

export default function Ani(props) {
  let distance = props.distance ? props.distance : "-10%";
  const [style, setS] = useState({
    opacity: 0,
    transition: ".3s",
    transform: !props.fixed && "translateX(" + distance + ")",
  });

  useEffect(() => {
    setTimeout(() => {
      setS({
        opacity: 1,
        transition: ".3s",
      });
    }, Math.random() * (800 - 150) + 150);
  }, []);

  return (
    <div style={style} className="ani-block">
      {props.children}
    </div>
  );
}
