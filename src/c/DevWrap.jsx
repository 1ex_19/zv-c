function Container(props) {
  return (
    <div className="container-fluid">
      <div className="row no-gutters">
        <div className="col">{props.children}</div>
      </div>
    </div>
  );
}

export default function DevWrap(props) {
  let development = process.env.NODE_ENV == "development";
  let col = props.col ?? "col-3";
  let children = props.container ? (
    <Container>{props.children}</Container>
  ) : (
    props.children
  );
  return development ? (
    <div className="container-fluid">
      <div className="row no-gutters">
        <div className={col}></div>
        <div className="col-9">{props.children}</div>
      </div>
    </div>
  ) : (
    children
  );
}
