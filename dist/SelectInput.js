"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Ul = /*#__PURE__*/function (_Component) {
  _inherits(Ul, _Component);

  var _super = _createSuper(Ul);

  function Ul(props) {
    var _this;

    _classCallCheck(this, Ul);

    _this = _super.call(this, props);
    _this.styles = {
      openStyle: {
        opacity: "1",
        transform: "translateY(0)",
        zIndex: "10"
      },
      closedStyle: {
        opacity: "0",
        transform: "translateY(-50%)",
        zIndex: "-1"
      }
    };
    _this.state = {
      // options: options,
      ulStyle: _this.styles.closedStyle
    };
    _this.show = _this.show.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(Ul, [{
    key: "show",
    value: function show() {
      var _this2 = this;

      setTimeout(function () {
        _this2.setState({
          ulStyle: _this2.styles.openStyle
        });
      }, 100);
    } // hide() {
    //   setTimeout(() => {
    //     this.setState({
    //       ulStyle: this.styles.closedStyle,
    //     })
    //   }, 200);
    // }

  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.show();
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("ul", {
        className: "shadow",
        style: this.state.ulStyle
      }, this.props.children);
    }
  }]);

  return Ul;
}(_react.Component);

var Select = /*#__PURE__*/function (_Component2) {
  _inherits(Select, _Component2);

  var _super2 = _createSuper(Select);

  function Select(props) {
    var _this3;

    _classCallCheck(this, Select);

    _this3 = _super2.call(this, props);
    _this3._input = /*#__PURE__*/_react.default.createRef();
    _this3.styles = {
      openStyle: {
        opacity: "1",
        transform: "translateY(0)",
        zIndex: "10"
      },
      closedStyle: {
        opacity: "0",
        transform: "translateY(-50%)",
        zIndex: "-1"
      }
    }; // let options = this.props.options && this.props.options.length > 0 && Array.isArray(this.props.options) ? this.props.options : []

    _this3.state = {
      // options: options,
      ulStyle: _this3.styles.closedStyle,
      value: _this3.props.placeholder,
      label: /*#__PURE__*/_react.default.createElement("div", {
        className: "text-secondary"
      }, _this3.props.placeholder),
      name: _this3.props.name,
      results: []
    }; // this.show = this.show.bind(this);
    // this.hide = this.hide.bind(this);

    _this3.createOption = _this3.createOption.bind(_assertThisInitialized(_this3));
    _this3.inputHandle = _this3.inputHandle.bind(_assertThisInitialized(_this3));
    _this3.getResults = _this3.getResults.bind(_assertThisInitialized(_this3));
    return _this3;
  }

  _createClass(Select, [{
    key: "getResults",
    value: function () {
      var _getResults = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(inputData) {
        var url, data, req, res;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.setState({
                  loading: true
                });
                url = this.props.url;
                data = this.props.data ? _objectSpread(_objectSpread({}, this.props.data), inputData) : inputData; // console.log(data);
                // console.log(url);

                _context.next = 5;
                return fetch(url, {
                  method: "POST",
                  body: JSON.stringify(data)
                });

              case 5:
                req = _context.sent;
                _context.next = 8;
                return req.json();

              case 8:
                res = _context.sent;

                if (res.ok) {
                  this.setState({
                    results: res.data // ulStyle: this.styles.openStyle

                  });
                } else {
                  this.setState({
                    results: [{
                      label: "no results",
                      value: "no results"
                    }]
                  });
                }

                this.setState({
                  loading: false
                });

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function getResults(_x) {
        return _getResults.apply(this, arguments);
      }

      return getResults;
    }()
  }, {
    key: "createOption",
    value: function createOption(item, index) {
      var _this4 = this;

      return /*#__PURE__*/_react.default.createElement("li", {
        key: index,
        onClick: function onClick(click) {
          // console.log(click);
          _this4._input.current.value = item.value;
          _this4.props.extraHandle && _this4.props.extraHandle(item.value);

          _this4.setState({
            label: item.label,
            value: item.value,
            results: []
          });
        }
      }, item.label);
    }
  }, {
    key: "blur",
    value: function blur(i) {// this.hide();
    }
  }, {
    key: "inputHandle",
    value: function inputHandle(i) {
      var value = i.currentTarget.value;

      if (value.length < 2) {
        this.setState({
          results: []
        });
        this.props.extraHandle && this.props.extraHandle("");
        return false;
      }

      this.getResults({
        input: value
      });
    }
  }, {
    key: "focus",
    value: function focus(i) {// this.show();
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props$pa, _this$props$style;

      var options = this.state.results.map(this.createOption);
      var _label = this.state.label;
      var pa = (_this$props$pa = this.props.pa) !== null && _this$props$pa !== void 0 ? _this$props$pa : window.zv.s.pa;
      var style = (_this$props$style = this.props.style) !== null && _this$props$style !== void 0 ? _this$props$style : {};
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "zv-input",
        style: _objectSpread({
          padding: pa
        }, style)
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "select-input-wrap position-relative"
      }, this.props.label && /*#__PURE__*/_react.default.createElement("label", {
        htmlFor: "_select_" + this.props.id,
        style: {
          color: window.zv.c.defLabel,
          marginLeft: ".25rem"
        },
        className: "mb-1 input-label"
      }, this.props.ico && /*#__PURE__*/_react.default.createElement("span", {
        style: {
          display: "inline-block",
          width: "1.1em",
          height: "100%",
          marginRight: ".5rem"
        }
      }, this.props.ico), this.props.label), /*#__PURE__*/_react.default.createElement("input", {
        autoComplete: "off",
        onInput: this.inputHandle,
        ref: this._input,
        type: "text",
        name: this.state.name,
        id: "_select_" + this.props.id,
        placeholder: this.props.placeholder ? this.props.placeholder : this.props.label,
        className: "form-control",
        onBlur: this.blur.bind(this),
        onFocus: this.focus.bind(this)
      }), options.length > 0 && /*#__PURE__*/_react.default.createElement(Ul, null, options)));
    }
  }]);

  return Select;
}(_react.Component);

var _default = Select;
exports.default = _default;