"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = DevWrap;

function Container(props) {
  return /*#__PURE__*/React.createElement("div", {
    className: "container-fluid"
  }, /*#__PURE__*/React.createElement("div", {
    className: "row no-gutters"
  }, /*#__PURE__*/React.createElement("div", {
    className: "col"
  }, props.children)));
}

function DevWrap(props) {
  var _props$col;

  var development = process.env.NODE_ENV == "development";
  var col = (_props$col = props.col) !== null && _props$col !== void 0 ? _props$col : "col-3";
  var children = props.container ? /*#__PURE__*/React.createElement(Container, null, props.children) : props.children;
  return development ? /*#__PURE__*/React.createElement("div", {
    className: "container-fluid"
  }, /*#__PURE__*/React.createElement("div", {
    className: "row no-gutters"
  }, /*#__PURE__*/React.createElement("div", {
    className: col
  }), /*#__PURE__*/React.createElement("div", {
    className: "col-9"
  }, props.children))) : children;
}