"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ElipsLoader;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function ElipsLoader(props) {
  var _props$style, _props$bg;

  var style = (_props$style = props.style) !== null && _props$style !== void 0 ? _props$style : {};
  var bg = (_props$bg = props.bg) !== null && _props$bg !== void 0 ? _props$bg : "#000";
  return /*#__PURE__*/React.createElement("div", {
    className: "lds-ellipsis"
  }, /*#__PURE__*/React.createElement("div", {
    style: _objectSpread({
      background: bg
    }, style)
  }), /*#__PURE__*/React.createElement("div", {
    style: _objectSpread({
      background: bg
    }, style)
  }), /*#__PURE__*/React.createElement("div", {
    style: _objectSpread({
      background: bg
    }, style)
  }), /*#__PURE__*/React.createElement("div", {
    style: _objectSpread({
      background: bg
    }, style)
  }));
}