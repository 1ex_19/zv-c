"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = OpenBlock;

var _react = _interopRequireWildcard(require("react"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function OpenBlock(props) {
  var block = /*#__PURE__*/_react.default.createRef();

  var _useState = (0, _react.useState)(true),
      _useState2 = _slicedToArray(_useState, 2),
      open = _useState2[0],
      setOpen = _useState2[1];

  var _useState3 = (0, _react.useState)(0),
      _useState4 = _slicedToArray(_useState3, 2),
      height = _useState4[0],
      setH = _useState4[1];

  var _useState5 = (0, _react.useState)({
    transition: ".3s",
    overflow: "hidden",
    marginTop: "10px",
    opacity: 1
  }),
      _useState6 = _slicedToArray(_useState5, 2),
      blockStyle = _useState6[0],
      setBS = _useState6[1];

  var _useState7 = (0, _react.useState)({
    transition: ".3s"
  }),
      _useState8 = _slicedToArray(_useState7, 2),
      icoStyle = _useState8[0],
      setIC = _useState8[1];

  var icon = /*#__PURE__*/_react.default.createElement("svg", {
    style: icoStyle,
    xmlns: "http://www.w3.org/2000/svg",
    height: "1.5em",
    width: "1.5em",
    fill: "currentColor",
    className: "bi bi-caret-down-fill",
    viewBox: "0 0 16 16"
  }, /*#__PURE__*/_react.default.createElement("path", {
    d: "M7.247 11.14L2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z"
  })); // useEffect(()=> {
  //   if (block.current.clientHeight != 0) {
  //     setH(block.current.clientHeight);
  //   }
  //   // setBS({
  //   //   transition: ".3s",
  //   //   overflow: "hidden",
  //   //   height: "0px"
  //   // });
  //   // setOpen(false);
  // });


  var toggle = function toggle() {
    if (open) {
      setBS({
        transition: ".3s",
        overflow: "hidden",
        height: "0px",
        marginTop: "0px"
      });
      setOpen(false);
      setIC({
        transition: ".3s"
      });
    } else {
      setBS({
        transition: ".3s",
        overflow: "hidden",
        marginTop: "10px" // height: height + "px"

      });
      setOpen(true);
      setIC({
        transform: "rotateZ(180deg)",
        transition: ".3s"
      });
    }
  };

  return /*#__PURE__*/_react.default.createElement("div", {
    className: "open-block"
  }, /*#__PURE__*/_react.default.createElement("div", {
    onClick: toggle,
    className: "open-block-title"
  }, /*#__PURE__*/_react.default.createElement("div", {
    className: "d-flex justify-content-between align-items-center"
  }, /*#__PURE__*/_react.default.createElement("span", null, props.title), /*#__PURE__*/_react.default.createElement("span", null, icon))), /*#__PURE__*/_react.default.createElement("div", {
    style: blockStyle,
    ref: block,
    className: "data"
  }, props.children));
}