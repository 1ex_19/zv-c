"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = LightBorder;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function LightBorder(props) {
  var _props$bc, _props$bw, _props$br, _props$pa, _props$style;

  var bc = (_props$bc = props.bc) !== null && _props$bc !== void 0 ? _props$bc : window.zv.c.defLight;
  var bw = (_props$bw = props.bw) !== null && _props$bw !== void 0 ? _props$bw : window.zv.s.bw;
  var br = (_props$br = props.br) !== null && _props$br !== void 0 ? _props$br : window.zv.s.br;
  var pa = (_props$pa = props.pa) !== null && _props$pa !== void 0 ? _props$pa : window.zv.s.pa;
  var style = (_props$style = props.style) !== null && _props$style !== void 0 ? _props$style : {};
  var c = props.hover ? "zv-light-border hover-handler" : "zv-light-border";
  return /*#__PURE__*/React.createElement("div", {
    style: _objectSpread({
      border: bw + " solid " + bc,
      borderRadius: br,
      padding: pa
    }, style),
    className: c
  }, props.children);
}