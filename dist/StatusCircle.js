"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var StatusCircle = /*#__PURE__*/function (_Component) {
  _inherits(StatusCircle, _Component);

  var _super = _createSuper(StatusCircle);

  function StatusCircle(props) {
    var _this;

    _classCallCheck(this, StatusCircle);

    _this = _super.call(this, props);
    _this._circle = /*#__PURE__*/_react.default.createRef();
    _this.radius = _this.props.size / 2 - _this.props.strokeWidth;
    _this.strokeDasharray = _this.radius * 2 * Math.PI;
    _this.state = {
      status: 0,
      secStatus: 0,
      color: "transparent",
      textStyle: {
        margin: 0,
        opacity: 0,
        transition: ".2s"
      }
    };
    _this.getStatus = _this.getStatus.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(StatusCircle, [{
    key: "getStatus",
    value: function getStatus() {
      var _this2 = this;

      var status = this.strokeDasharray / 100 * (100 - this.props.status);
      var secStatus = this.strokeDasharray - status;
      setTimeout(function () {
        _this2.setState({
          status: status,
          secStatus: _this2.strokeDasharray - status,
          // color: this.props.color,
          color: "url(#" + _this2.props.id + "_gradient)",
          textStyle: {
            margin: 0,
            opacity: 1,
            transition: ".2s",
            fontSize: _this2.props.size / 6 + "px"
          }
        });
      }, 500);
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.getStatus();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prev) {
      if (prev.status !== this.props.status) {
        this.getStatus();
      }
    }
  }, {
    key: "render",
    value: function render() {
      // let radius = (this.props.size / 2) - (this.props.strokeWidth * 2);
      // let strokeDasharray = radius * 2 * Math.PI;
      // let status = (strokeDasharray / 100) * (100 - this.props.status);
      // let secStatus = strokeDasharray - status
      // console.log(this.props.status);
      return /*#__PURE__*/_react.default.createElement("span", {
        className: "svg-wrap",
        style: {
          height: this.props.size + "px",
          width: this.props.size + "px",
          position: "relative",
          display: "block"
        }
      }, /*#__PURE__*/_react.default.createElement("span", {
        className: "text-wrap",
        style: {
          position: "absolute",
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100%"
        }
      }, /*#__PURE__*/_react.default.createElement("p", {
        style: this.state.textStyle
      }, /*#__PURE__*/_react.default.createElement("strong", null, this.props.inner))), /*#__PURE__*/_react.default.createElement("svg", {
        className: "progress-ring",
        style: {
          background: this.props.bg
        },
        height: this.props.size + "px",
        width: this.props.size + "px"
      }, /*#__PURE__*/_react.default.createElement("circle", {
        ref: this._circle,
        className: "progress-ring__circle",
        style: {
          transition: "stroke-dashoffset 0.35s",
          transform: "rotate(-90deg)",
          transformOrigin: "50% 50%",
          strokeDasharray: this.strokeDasharray + " " + this.strokeDasharray,
          strokeDashoffset: this.state.status
        },
        stroke: this.state.color,
        strokeWidth: this.props.strokeWidth,
        fill: "transparent",
        r: this.radius,
        cx: this.props.size / 2,
        cy: this.props.size / 2
      }), /*#__PURE__*/_react.default.createElement("linearGradient", {
        id: this.props.id + "_gradient",
        x1: "0%",
        y1: "0%",
        x2: "0%",
        y2: "100%"
      }, /*#__PURE__*/_react.default.createElement("stop", {
        offset: "0%",
        stopColor: this.props.startColor
      }), /*#__PURE__*/_react.default.createElement("stop", {
        offset: "100%",
        stopColor: this.props.endColor
      })), /*#__PURE__*/_react.default.createElement("circle", {
        ref: this._circle,
        className: "progress-ring__circle",
        style: {
          transition: "stroke-dashoffset 0.35s",
          transform: "rotate(-90deg)",
          transformOrigin: "50% 50%",
          strokeDasharray: this.strokeDasharray + " " + this.strokeDasharray,
          strokeDashoffset: -this.state.secStatus
        },
        stroke: this.props.secondaryColor,
        strokeWidth: this.props.strokeWidth,
        fill: "transparent",
        r: this.radius,
        cx: this.props.size / 2,
        cy: this.props.size / 2
      })));
    }
  }]);

  return StatusCircle;
}(_react.Component);

var _default = StatusCircle;
exports.default = _default;