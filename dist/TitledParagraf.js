"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = TitledParagraf;

function TitledParagraf(props) {
  return /*#__PURE__*/React.createElement("div", {
    className: "col-12"
  }, /*#__PURE__*/React.createElement("div", {
    className: "inner-col p-2"
  }, /*#__PURE__*/React.createElement("div", {
    className: "one-record"
  }, props.title && /*#__PURE__*/React.createElement("div", {
    className: "title"
  }, /*#__PURE__*/React.createElement("h5", {
    className: "text-secondary"
  }, props.title, props.dotted && ":")), /*#__PURE__*/React.createElement("div", {
    className: "inner main-blue-bg p-2"
  }, props.children))));
}