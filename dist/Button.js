"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = Btn;

function Btn(props) {
  var _props$locked, _ref, _props$locked$bootTyp, _props$bootType, _props$type, _props$style, _props$clickStyle, _props$locked$text;

  var locked = (_props$locked = props.locked) !== null && _props$locked !== void 0 ? _props$locked : {};
  var bootType = props.locked.state ? (_ref = (_props$locked$bootTyp = props.locked.bootType) !== null && _props$locked$bootTyp !== void 0 ? _props$locked$bootTyp : props.bootType) !== null && _ref !== void 0 ? _ref : "secondary" : (_props$bootType = props.bootType) !== null && _props$bootType !== void 0 ? _props$bootType : "secondary";
  var defClass = props.click ? "btn touch-clicked" : "btn";
  var type = (_props$type = props.type) !== null && _props$type !== void 0 ? _props$type : "submit";
  var style = (_props$style = props.style) !== null && _props$style !== void 0 ? _props$style : {};
  var clickStyle = (_props$clickStyle = props.clickStyle) !== null && _props$clickStyle !== void 0 ? _props$clickStyle : {};
  var s = props.click ? clickStyle : style;
  return /*#__PURE__*/React.createElement("button", {
    style: s,
    type: type,
    disabled: props.locked.state,
    onClick: function onClick(e) {
      return props.onClick && props.onClick(e);
    },
    className: defClass + " btn-" + bootType
  }, props.locked.state ? (_props$locked$text = props.locked.text) !== null && _props$locked$text !== void 0 ? _props$locked$text : props.children : props.children);
}